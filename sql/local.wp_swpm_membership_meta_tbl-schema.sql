/*!40101 SET NAMES binary*/;
/*!40014 SET FOREIGN_KEY_CHECKS=0*/;

CREATE TABLE `wp_swpm_membership_meta_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_label` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` text COLLATE utf8mb4_unicode_520_ci,
  `meta_type` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'text',
  `meta_default` text COLLATE utf8mb4_unicode_520_ci,
  `meta_context` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'default',
  PRIMARY KEY (`id`),
  KEY `level_id` (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
