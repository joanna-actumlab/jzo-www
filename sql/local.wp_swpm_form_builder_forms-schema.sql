/*!40101 SET NAMES binary*/;
/*!40014 SET FOREIGN_KEY_CHECKS=0*/;

CREATE TABLE `wp_swpm_form_builder_forms` (
  `form_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `form_key` tinytext NOT NULL,
  `form_title` text NOT NULL,
  `form_type` tinyint(4) DEFAULT '0',
  `form_membership_level` int(11) DEFAULT '0',
  `form_success_type` varchar(25) DEFAULT 'text',
  `form_success_message` text,
  `form_notification_setting` varchar(25) DEFAULT NULL,
  `form_notification_email_name` varchar(255) DEFAULT NULL,
  `form_notification_subject` varchar(255) DEFAULT NULL,
  `form_notification_message` text,
  `form_label_alignment` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`form_id`),
  UNIQUE KEY `form_unique_key_id` (`form_type`,`form_membership_level`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
