/*!40101 SET NAMES binary*/;
/*!40014 SET FOREIGN_KEY_CHECKS=0*/;
/*!40103 SET TIME_ZONE='+00:00' */;
INSERT INTO `wp_swpm_form_builder_fields` VALUES
(1,1,"fieldset","fieldset","","","Rejestracja",0,0,"","","","","","",0,0,0),
(2,1,"user_name","text","","","Login",1,0,"","yes","medium","","","",0,0,0),
(3,1,"password","password","","","Hasło",5,0,"","no","medium","","","",0,0,0),
(4,1,"membership_level","text","","","Membership",6,0,"","no","medium","","","",1,0,0),
(5,1,"primary_email","email","","","Adres e-mail",2,0,"email","yes","medium","","","",0,0,0),
(8,1,"submit","submit","","","Załóż konto",43,0,"","","","","","",0,0,0),
(17,1,"first_name","text","","","Imię",3,0,"","no","medium","","","",0,0,0),
(19,1,"last_name","text","","","Nazwisko",4,0,"","no","medium","","","",0,0,0),
(37,1,"fieldset","fieldset","","","Dane adresowe",16,0,"","","","","","",0,0,0),
(39,1,"custom","text","","","Ulica",19,0,"","yes","medium","","","",0,0,0),
(41,1,"custom","text","","","Kod pocztowy",22,0,"","no","large","","","",0,0,0),
(43,1,"custom","text","","","Nr domu",25,0,"","no","medium","","","",0,0,0),
(45,1,"custom","text","","","Nr mieszkania",28,0,"","no","medium","","","",0,0,0),
(47,1,"custom","text","","","Funkcja",31,0,"","no","medium","","","",0,0,0),
(49,1,"custom","text","","","Miasto",34,0,"","yes","medium","","","",0,0,0),
(53,1,"custom","phone","","","Numer telefonu",40,0,"phone","yes","medium","","","",0,0,0),
(56,4,"user_name","text","","","Login",1,0,"","yes","medium","","","",0,0,2),
(57,4,"primary_email","email","","","Adres e-mail",2,0,"email","yes","medium","","","",0,0,5),
(58,4,"first_name","text","","","Imię",3,0,"","no","medium","","","",0,0,17),
(59,4,"last_name","text","","","Nazwisko",4,0,"","no","medium","","","",0,0,19),
(60,4,"password","password","","","Hasło",5,0,"","no","medium","","","",0,0,3),
(61,4,"membership_level","text","","","Membership",6,0,"","no","medium","","","",1,0,4),
(63,4,"custom","text","","","Ulica",7,0,"","yes","medium","","","",0,0,39),
(64,4,"custom","text","","","Kod pocztowy",8,0,"","no","large","","","",0,0,41),
(65,4,"custom","text","","","Nr domu",9,0,"","no","medium","","","",0,0,43),
(66,4,"custom","text","","","Nr mieszkania",10,0,"","no","medium","","","",0,0,45),
(68,4,"custom","text","","","Miasto",12,0,"","yes","medium","","","",0,0,49),
(69,4,"custom","phone","","","Numer telefonu",13,0,"phone","yes","medium","","","",0,0,53),
(70,4,"submit","submit","","","Zapisz zmiany",55,0,"","","","","","",0,0,8),
(71,4,"fieldset","fieldset","","","Edytuj Profil",0,0,"","","","","","",0,0,0),
(72,4,"","","","","Rejestracja",46,0,"","no","small","","","",0,0,1),
(73,4,"","","","","Dane adresowe",49,0,"","no","small","","","",0,0,37),
(74,4,"","","","","Funkcja",52,0,"","no","medium","","","",0,0,47);
