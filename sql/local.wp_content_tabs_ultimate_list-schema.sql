/*!40101 SET NAMES binary*/;
/*!40014 SET FOREIGN_KEY_CHECKS=0*/;

CREATE TABLE `wp_content_tabs_ultimate_list` (
  `id` mediumint(5) NOT NULL AUTO_INCREMENT,
  `styleid` mediumint(6) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_520_ci,
  `files` text COLLATE utf8mb4_unicode_520_ci,
  `css` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
