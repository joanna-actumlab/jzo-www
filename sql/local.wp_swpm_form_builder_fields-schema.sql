/*!40101 SET NAMES binary*/;
/*!40014 SET FOREIGN_KEY_CHECKS=0*/;

CREATE TABLE `wp_swpm_form_builder_fields` (
  `field_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `form_id` bigint(20) NOT NULL,
  `field_key` varchar(255) NOT NULL,
  `field_type` varchar(25) NOT NULL,
  `field_options` text,
  `field_description` text,
  `field_name` text NOT NULL,
  `field_sequence` bigint(20) DEFAULT '0',
  `field_parent` bigint(20) DEFAULT '0',
  `field_validation` varchar(25) DEFAULT NULL,
  `field_required` varchar(25) DEFAULT NULL,
  `field_size` varchar(25) DEFAULT 'medium',
  `field_css` varchar(255) DEFAULT NULL,
  `field_layout` varchar(255) DEFAULT NULL,
  `field_default` text,
  `field_adminonly` tinyint(4) DEFAULT '0',
  `field_readonly` tinyint(4) DEFAULT '0',
  `reg_field_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;
