/*!40101 SET NAMES binary*/;
/*!40014 SET FOREIGN_KEY_CHECKS=0*/;

CREATE TABLE `wp_swpm_payments_tbl` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `first_name` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `last_name` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `member_id` varchar(16) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `membership_level` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `txn_date` date NOT NULL DEFAULT '0000-00-00',
  `txn_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `subscr_id` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `reference` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `payment_amount` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `gateway` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `status` varchar(16) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `ip_address` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
