/*!40101 SET NAMES binary*/;
/*!40014 SET FOREIGN_KEY_CHECKS=0*/;
/*!40103 SET TIME_ZONE='+00:00' */;
INSERT INTO `wp_terms` VALUES
(1,"Uncategorized","uncategorized",0),
(2,"Basic Menu","basic-menu",0),
(3,"Serwis ogólny","serwis-ogolny",0),
(4,"Serwis dla optyków","serwis-dla-optykow",0),
(18,"Soczewki rozwiązania","soczewki-rozwiazania",0),
(19,"Soczewki potrzeby","soczewki-potrzeby",0),
(34,"Jak dbać o wzrok?","jak-dbac-o-wzrok",0),
(35,"Jak wybrać soczewki?","jak-wybrac-soczewki",0),
(36,"Soczewki konstrukcje","soczewki-konstrukcje",0),
(37,"Soczewki materiały","soczewki-materialy",0),
(38,"Uszlachetnienia","uszlachetnienia",0),
(39,"Salon optyczny","salon-optyczny",0),
(40,"Oprawy","oprawy",0),
(41,"Usługi i szkolenia","uslugi-i-szkolenia",0),
(42,"Aplikacje","aplikacje",0),
(43,"Materiały","materialy",0),
(44,"Izooptyka (Drzewo Wiedzy)","izooptyka-drzewo-wiedzy",0),
(46,"O JZO","o-jzo",0),
(47,"Main Menu","main-menu",0),
(48,"Soczewki progresywne","soczewki-progresywne",0),
(49,"Soczewki dla kierowców","soczewki-dla-kierowcow",0),
(50,"Soczewki dla aktywnych","soczewki-dla-aktywnych",0),
(51,"Soczewki do pracy przy komputerze","soczewki-do-pracy-przy-komputerze",0),
(52,"Soczewki dla dzieci","soczewki-dla-dzieci",0),
(53,"Soczewki przy dużej wadzie wzroku","soczewki-przy-duzej-wadzie-wzroku",0),
(54,"second-menu","second-menu",0),
(55,"Login Panel","login-panel",0),
(56,"Sliders","sliders",0),
(57,"Slider-1","slider-1",0),
(58,"Logowanie i Rejestracja","logowanie-i-rejestracja",0),
(59,"Slider-2","slider-2",0);
