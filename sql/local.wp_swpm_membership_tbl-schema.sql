/*!40101 SET NAMES binary*/;
/*!40014 SET FOREIGN_KEY_CHECKS=0*/;

CREATE TABLE `wp_swpm_membership_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(127) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'subscriber',
  `permissions` tinyint(4) NOT NULL DEFAULT '0',
  `subscription_period` varchar(11) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '-1',
  `subscription_duration_type` tinyint(4) NOT NULL DEFAULT '0',
  `subscription_unit` varchar(20) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `loginredirect_page` text COLLATE utf8mb4_unicode_520_ci,
  `category_list` longtext COLLATE utf8mb4_unicode_520_ci,
  `page_list` longtext COLLATE utf8mb4_unicode_520_ci,
  `post_list` longtext COLLATE utf8mb4_unicode_520_ci,
  `comment_list` longtext COLLATE utf8mb4_unicode_520_ci,
  `attachment_list` longtext COLLATE utf8mb4_unicode_520_ci,
  `custom_post_list` longtext COLLATE utf8mb4_unicode_520_ci,
  `disable_bookmark_list` longtext COLLATE utf8mb4_unicode_520_ci,
  `options` longtext COLLATE utf8mb4_unicode_520_ci,
  `protect_older_posts` tinyint(1) NOT NULL DEFAULT '0',
  `campaign_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
