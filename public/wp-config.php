<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'BD9I8dFI72tCB4CJXeOCYvodZlUlI+p/gsGmrvlslJFOodFtqXgAju5MAMv74Rho3jqsHRUrfJCU5CgoUFHgjA==');
define('SECURE_AUTH_KEY',  'MqJdTETkAMFj4iWD2P6M5tNYxXfqFhz7Rr12RN7VJ4pzwgnmoQrqmEZwlJCtSvQX1+CmNrd8JeWCZg9/51ipxw==');
define('LOGGED_IN_KEY',    '5ACbgignhvkQh18I6tfbUn4UDQip9Oe9NepzPu5Mz4OGjUHFDPNXODEYO9sKOxVRkQrVPpM8PkaTQWMUoG0GZw==');
define('NONCE_KEY',        'HIe/6L9tY14cXDGeqLiR1o6XhCnJXEuICzIqYpjezmqn1MYYUrCUR3+hKbogn6Db104djCL9VjiTYAHKFfUrmA==');
define('AUTH_SALT',        'aHzmgsxgB4IaxVVRwUAfuK3mBt6gYTPxV4CeMExGvHZk2ePu2F5OIhlp6ZYhS8GM3EPK2ASO9MNQOt8o3KDMYQ==');
define('SECURE_AUTH_SALT', 'qlaTWBS19uYN2LAM8xhoEQSPm9ltaCM40PwolIV0KswVwotnoV+2nrBNAdB0CsUF94Bup7OKOw1SaRWx7z+xHQ==');
define('LOGGED_IN_SALT',   'JEMfFn1rcnVcPKULwX8Ie0ER/02GQgARPrSsw62XAhTYkqizFeUaVx7XTRI438yOxNNpObglWEMyQtj2oNmSIg==');
define('NONCE_SALT',       'MbmyyH2u3rexUEaA9uul4E1Sm2fVeNE1syN/sQc918qzVClxvhSTidUnb5vOD+u/02P5jOg6LSn9LXilWwdb8g==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
