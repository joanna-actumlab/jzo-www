<?php

function swpm_mda_create_user_details_page() {
    //$swpm_member_info_page_content = '<p>Sample Member Info Page</p>';
    $swpm_member_info_page_content = "[swpm_show_member_profile]";

    $member_info_page = array(
        'post_title' => 'Member Info',
        'post_name' => 'member-info',
        'post_content' => $swpm_member_info_page_content,
        'post_parent' => 0,
        'post_status' => 'publish',
        'post_type' => 'page',
        'comment_status' => 'closed',
        'ping_status' => 'closed'
    );

    $member_info_page_obj = get_page_by_path('member-info');
    if (!$member_info_page_obj) {
        $member_info_page_id = wp_insert_post($member_info_page);
    } else {
        $member_info_page_id = $member_info_page_obj->ID;
        if ($member_info_page_obj->post_status == 'trash') { //For cases where page may be in trash, bring it out of trash
            wp_update_post(array('ID' => $member_info_page_obj->ID, 'post_status' => 'publish'));
        }
    }
    $swpm_member_info_page_permalink = get_permalink($member_info_page_id);
    $options = array(
        'member-profile-url' => $swpm_member_info_page_permalink,
    );
    update_option('swpm_mda_settings', $options); //store the results in WP options table
}

function swpm_mda_output_page_ids_as_hidden_input_in_form() {
    //This will add the page_id or post_id values in the form as hidden inputs. So the search submission via GET method can work correctly.
    //So search submission on default permalink will look like this: http://www.example.com/?page_id=123&swpm_searchq=testuser
    $output = '';
    $keys = array('page_id', 'p');
    foreach ($keys as $name) {
        if (!isset($_GET[$name])) {
            continue;
        }
        $value = sanitize_text_field($_GET[$name]);
        $name = sanitize_text_field($name);
        $output .= '<input type="hidden" name="' . $name . '" value="' . $value . '">';
    }
    return $output;
}
