<?php

add_shortcode('swpm_show_member_listing', 'swpm_show_member_listing_handler');
add_shortcode('swpm_show_member_profile', 'swpm_show_member_profile_handler');
add_filter('query_vars', 'swpm_sc_add_query_vars_filter');

/*
  Function below shows member profile on member-info page.
  Shortcode is [swpm_show_member_profile]
  Default options are are: show everything, except profile button and search box.

  For example, if you don't want to show member's email, you can put it like this
  [swpm_show_member_profile email="0"]

  If you don't want email, phone number and adress to be displayed:
  [swpm_show_member_profile email="0" phone="0" full_address="0"]
 */

function swpm_show_member_profile_handler($atts) {
    $params = shortcode_atts(array(
        'member_id' => '0',
        'user_name' => '1',
        'first_name' => '1',
        'last_name' => '1',
        'membership_level' => '1',
        'account_state' => '0',
        'full_address' => '0',
        'country' => '1',
        'phone' => '1',
        'email' => '1',
        'member_since' => '1',
        'gender' => '0',
        'profile_image' => '0',
        'show_custom_fields' => '',
	'company_name' => '0',
            ), $atts, 'swpm_show_member_profile');

    $member_id = intval(get_query_var('swpm_member_id', null));

    $output = swpm_mda_show_member_profile($params, $member_id);

    return $output;
}

/*
  Function below lists members.
  Shortcode is [swpm_show_member_listing]
  By default, 30 members per page are displayed; order is unsorted; only membership_level and country is displayed;
  profile button and search box are visible;
 */

function swpm_show_member_listing_handler($atts) {

    $output = swpm_mda_show_listing($atts);

    return $output;
}

function swpm_sc_add_query_vars_filter($vars) {
    $vars[] = "swpm_page";
    $vars[] = "swpm_searchq";
    $vars[] = "swpm_member_id";
    return $vars;
}
