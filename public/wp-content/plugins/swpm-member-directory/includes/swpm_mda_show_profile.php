<?php

function swpm_mda_show_member_profile($params, $member_id) {

    wp_register_style('swpm_profile_details_style', SWPM_MDA_URL . '/css/swpm-mda-profile-details.css'); //load styles
    wp_enqueue_style('swpm_profile_details_style');

    global $wpdb;
    $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}swpm_members_tbl WHERE member_id = %d", $member_id);
    $member_record = $wpdb->get_row($query);

    $output = '';
    if ($member_record == null) {
        $output .= 'You need to click on the view profile link of a member to view the details.';
    } else {
        $output .= swpm_mda_render_member_details($member_record, $params);
    }

    return $output;
}

function swpm_mda_render_member_details($member, $params) {
    $member_id = $member->member_id;

    $field_names = array(// those are human-readable names for tables. For example, Instead of showing 'user_name', it would be better to show "Username"
        'member_id' => __('Member ID', 'simple-membership'),
        'user_name' => __('Username', 'simple-membership'),
        'first_name' => __('First Name', 'simple-membership'),
        'last_name' => __('Last Name', 'simple-membership'),
        'membership_level' => __('Level', 'simple-membership'),
        'account_state' => __('Account State', 'simple-membership'),
        'full_address' => __('Address', 'simple-membership'),
        'country' => __('Country', 'simple-membership'),
        'phone' => __('Phone', 'simple-membership'),
        'email' => __('Email', 'simple-membership'),
        'member_since' => __('Member Since', 'simple-membership'),
        'gender' => __('Gender', 'simple-membership'),
	'company_name' => __('Company', 'simple-membership'),
    );

    $ignored_fields = array(
        'last_accessed_from_ip' => '',
        'profile_image' => '',
    );

    $output = '';
    $output .= '<div class="swpm_mda_member-details swpm_mda_membership-lvl-' . $member->membership_level . ' swpm_mda_member-id-' . $member->member_id . '">';
    if ($params['profile_image'] == 1) {
        $output .= '<div class="swpm_mda_member-details-profile-image">';
        $output.=get_avatar($member->email, 128, '', false, array("force_display" => true));
        $output .= '</div>';
    }

    $output .= '<div class="swpm_mda_member-details-core-fields">';
    $output .= '<div class="swpm_mda_member-details-lines">';
    foreach ($params as $key => $value) {
        if ($value == '1') {
            if (!isset($ignored_fields[$key])) {
                //This is not an ignored field. Lets see if we need to show this in the listing entry.
                if (!isset($field_names[$key])) {
                    //This is not one of the core fields (human readable fields) that can be shown in the entry.
                    //Go to the next entry
                    continue;
                }

                $field_name = $field_names[$key];

                if (isset($member->$key)) {
                    $field_value = $member->$key;
                } else {
                    $field_value = ' - ';
                }

                if ($key == 'membership_level' && is_numeric($member->$key)) {
                    //Find the membership level name from the ID so we can show the name instead of the ID value.
                    $level_row = SwpmUtils::get_membership_level_row_by_id($member->$key);
                    $field_value = $level_row->alias;
                }
                if ($key == 'full_address') {
                    $full_address = "";
                    $full_address .= $member->address_street;
                    if (!empty($member->address_city)) {
                        $full_address .= ", " . $member->address_city;
                    }
                    if (!empty($member->address_state)) {
                        $full_address .= ", " . $member->address_state;
                    }
                    if (!empty($member->address_zipcode)) {
                        $full_address .= ", " . $member->address_zipcode;
                    }
                    $field_value = $full_address;
                }

                $output .= '<div class="swpm_mda_member-details-row">';
                $output .= '<div class="swpm_mda_member-details-label">' . $field_name . '</div>';
                $output .= '<div class="swpm_mda_member-details-value">' . $field_value . '</div>';
                $output .= '</div>';
            }
        }
    }
    $output .= '</div>'; //end of .swpm_mda_member-details-lines
    $output .= '</div>'; //end of .swpm_mda_member-details-core-fields
    
    /*************** Form Builder Custom Fields *****************/
    // Handle custom fields if form builder is active and the custom field option is enabled in the shortcode.
    if (class_exists('Swpm_Form_Builder') && !empty($params['show_custom_fields'])) {

        if (version_compare(SWPMFB_VERSION, '4.3.2', '<')) {
            echo '<div style="color: red;"><p>Attention! You need to upgrade your Form Builder addon to a more recent version to use the custom fields feature with the member directory addon.</p></div>';
            return;
        }

        $custom_fields_set = SwpmFbUtilsCustomFields::get_custom_fields_set_for_given_member($member_id);
        $custom_data = SwpmFbUtilsCustomFields::get_custom_data_by_member_id($member_id);

        //Uncomment to see the field set data.
        //echo '<pre>';
        //print_r($custom_fields_set);
        //echo '</pre>';

        $output .= '<div class="swpm_mda_member-details-custom-fields">';
        foreach ($custom_fields_set as $fieldmeta) {
	    //check if this is admin only field
	    if ($fieldmeta->adminonly==="1") {
		continue;
	    }
	    
            $field_type = $fieldmeta->type;
            $field_name = $fieldmeta->name;

            $field_value = '';
            if (isset($custom_data[$field_name]) && !empty($custom_data[$field_name])) {
                $field_value = $custom_data[$field_name];
            }
	    
            if ($field_type == 'file-upload') {
                //Nothing to do for file-upload type fields.
                continue; //Go to the next field
            }

            if ($field_type == 'url') {
		//Use the URL field's value to create a link.
		$url_field_output = '<a href="'.$field_value.'" target="_blank">'.$field_value.'</a>';
		
		//Use a filter to allow overriding of the URL field output.
		$url_field_output = apply_filters('swpm_mda_profile_url_field_value', $url_field_output, $field_value);
		$field_value = $url_field_output;
            }
	    
	    if ($field_type == 'address') {
		$address_obj = json_decode($field_value, true);

		$full_address = "";
		$full_address .= $address_obj['address'];
//		if (!empty($address_obj['address-2'])) {
//		    $full_address .= ", " . $address_obj['address-2'];
//		}		    
		if (!empty($address_obj['city'])) {
		    $full_address .= ", " . $address_obj['city'];
		}
		if (!empty($address_obj['state'])) {
		    $full_address .= ", " . $address_obj['state'];
		}
		if (!empty($address_obj['zip'])) {
		    $full_address .= ", " . $address_obj['zip'];
		}
		if (!empty($address_obj['country'])) {
		    $full_address .= ", " . $address_obj['country'];
		}
		$field_value = $full_address;
	    }
	    
            if ($field_type == 'checkbox') {
                //Handle the display of checkbox type fields
                $checkbox_options = maybe_unserialize($fieldmeta->options);
                $saved_values = maybe_unserialize(empty($field_value) ? '' : $field_value);
                $selected_values = array_values((array) $saved_values);

                $output .= '<div class="swpm_mda_member-details-row">';
                $output .= '<div class="swpm_mda_member-details-label">' . $field_name . '</div>';
                $output .= '<div class="swpm_mda_member-details-value">';
		$out='';
                    foreach ($selected_values as $key => $value) {
                        $out .= $value==="1" ? (isset($checkbox_options[$key]) ? $checkbox_options[$key].', ' : '') : '';
                    }
		if (!empty($out)) {
		    $output.=rtrim($out,', ');
                } else {
                    $output .= ' - ';
                }
                $output .= '</div>'; //end of .swpm_mda_member-details-value
                $output .= '</div>';
                continue; //Go to the next field
            }

            //Handle the display of all other types of fields.
            $output .= '<div class="swpm_mda_member-details-row">';
            $output .= '<div class="swpm_mda_member-details-label">' . $field_name . '</div>';
            $output .= '<div class="swpm_mda_member-details-value">';
            if (empty($field_value)) {
                $output .= ' - ';
            } else {
                $output .= $field_value;
            }
            $output .= '</div>'; //end of .swpm_mda_member-details-value
            $output .= '</div>';
        }

        $output .= '</div>'; //end of .swpm_mda_member-details-custom-fields
    }

    $output .= '</div>'; //end of .swpm_mda_member-details
    return $output;
}
