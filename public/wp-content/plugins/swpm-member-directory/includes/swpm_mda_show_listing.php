<?php

function swpm_mda_show_listing( $params, $member_id = null ) {

    //Template
    if ( isset( $params[ 'template' ] ) && $params[ 'template' ] == '2' ) {
	include_once(SWPM_MDA_PATH . '/views/template-2.php');
    } else {
	include_once(SWPM_MDA_PATH . '/views/template-1.php');
    }

    //Handle sorting
    $order_str = ' ORDER BY mem.member_id'; //By default sort by member_id (basically in the order the entries were added to the site).
    switch ( strtolower( $params[ 'sort_by' ] ) ) {
	case 'user_name': // sort by user name
	    $order_str	 = ' ORDER BY mem.user_name';
	    break;
	case 'membership_level': // sort by membership level alias (name)
	    $order_str	 = ' ORDER BY lvl.alias';
	    break;
	case 'full_name': // sort by full_name, e.g. Johm Smith
	    $order_str	 = ' ORDER BY full_name';
	    break;
	case 'last_name'; // sort by last name only, e.g. Smith
	    $order_str	 = ' ORDER BY mem.last_name';
	    break;
    } //more sort options could be added if needed
    //Handle sort order
    $sort_order = '';
    switch ( strtolower( $params[ 'sort_order' ] ) ) {
	case 'asc': // ascending sort order (default, so there is no need to put it in every shortcode), e.g. 1,2,3,4,5
	    $sort_order	 = ' ASC';
	    break;
	case 'desc': // descending (reverse) sort order, e.g. 5,4,3,2,1
	    $sort_order	 = ' DESC';
	    break;
    }

    $order_str .= $sort_order;

    //Handle where clause
    $where_clause = '';
    if ( ! empty( $params[ 'from_levels' ] ) ) {
	$levels		 = preg_replace( '/\ /', '', $params[ 'from_levels' ] ); // remove spaces so parameters like "1, 2, 3" would become "1,2,3"
	$levels		 = explode( ',', $levels );
	$add_cond	 = '';
	foreach ( $levels as $level ) {
	    $add_cond .= 'mem.membership_level = "' . $level . '" OR ';
	}
	$add_cond	 = substr( $add_cond, 0, -4 ); //remove last ' OR '
	$add_cond	 = ' AND (' . $add_cond . ')';
	$where_clause	 .= $add_cond;
    }

    if ( ! empty( $params[ 'from_account_status' ] ) ) {
	$states		 = preg_replace( '/\ /', '', $params[ 'from_account_status' ] ); // remove spaces so parameters like "active, incative, expired" would become "active,inactive,expired"
	$states		 = explode( ',', $states );
	$add_cond	 = '';
	foreach ( $states as $state ) {
	    $add_cond .= 'mem.account_state = "' . $state . '" OR ';
	}
	$add_cond	 = substr( $add_cond, 0, -4 ); //remove last ' OR '
	$add_cond	 = ' AND (' . $add_cond . ')';
	$where_clause	 .= $add_cond;
    }

    $search_str = get_query_var( 'swpm_searchq', null );
    /*
      By default, it searches the term in user_name, last_name and first_name fields only. If you want it, for example, to also search in email field,
      you need to add this: OR mem.email LIKE "%'.$search_term.'%".
     */
    if ( isset( $search_str ) && $search_str != '' ) {
	$search_term	 = htmlentities( $search_str );
	$search_str	 = ' AND (mem.user_name LIKE "%' . $search_term . '%" '
	. 'OR mem.last_name LIKE "%' . $search_term . '%" '
	. 'OR mem.first_name LIKE "%' . $search_term . '%"'
	. 'OR mem.email LIKE "%' . $search_term . '%"'
	. 'OR mem.company_name like "%' . $search_term . '%")';
    }

    $pagin_str = '';

    $page_num = intval( get_query_var( 'swpm_page', 1 ) );

    $items_per_page	 = intval( $params[ 'items_per_page' ] );
    $pagin_str	 = ' LIMIT ' . ($page_num - 1) * $items_per_page . ', ' . $items_per_page;

    global $wpdb;

    $the_query = 'SELECT SQL_CALC_FOUND_ROWS mem.*, mem.membership_level AS membership_lvl_id, CONCAT_WS(" ",mem.address_street,mem.address_city,mem.address_state,mem.address_zipcode) AS full_address, CONCAT_WS(" ",first_name,last_name) AS full_name, lvl.alias AS membership_level FROM ' . $wpdb->prefix . 'swpm_members_tbl AS mem, ' . $wpdb->prefix . 'swpm_membership_tbl AS lvl WHERE (mem.membership_level = lvl.id)';

    $members = $wpdb->get_results( $the_query . $search_str . $where_clause . $order_str . $pagin_str );

    $found_rows = $wpdb->get_var( 'SELECT FOUND_ROWS()' );

    if ( ! $params[ 'search_box' ] == '1' ) {
	$tpl[ 'search_box' ] = '';
    }

    $tpl[ 'clear_search_url' ] = esc_url( remove_query_arg( array( 'swpm_searchq', 'swpm_page' ) ) );

    $search_res_text = '';

    if ( $members == null ) {
	if ( isset( $search_term ) ) {
	    $search_res_text = 'Nothing found for "' . $search_term . '".';
	} else {
	    $search_res_text		 = 'No members found.';
	    $tpl[ 'clear_search_button' ]	 = '';
	}
    } else {
	if ( isset( $search_term ) && $search_term != '' ) {
	    $search_res_text = 'Search results for "' . htmlentities( $search_term ) . '".';
	} else {
	    $tpl[ 'clear_search_button' ] = '';
	}
	$tpl[ 'members_list' ]	 .= $tpl[ 'members_row_start' ];
	$i			 = $tpl[ 'members_per_row' ]; //items per row
	foreach ( $members as $member ) {
	    $i --;
	    if ( $i < 0 ) { //new row
		$tpl[ 'members_list' ]	 .= $tpl[ 'members_row_end' ];
		$tpl[ 'members_list' ]	 .= $tpl[ 'members_row_start' ];
		$i			 = $tpl[ 'members_per_row' ];
		$i --;
	    }
	    $tpl[ 'members_list' ] .= swpm_mda_get_member_item_tpl( $member, $params, $tpl );
	}
	$tpl[ 'members_list' ] .= $tpl[ 'members_row_end' ]; //close row div
    }

    $tpl[ 'search_result_text' ] = $search_res_text;

    $tpl[ 'pagination_items' ] = '';

    if ( $found_rows != 0 ) {
	$pages = ceil( $found_rows / $items_per_page );
	if ( $pages > 1 ) {
	    $i = 1;
	    while ( $i <= $pages ) {
		if ( $i != $page_num ) {
		    $url	 = esc_url( add_query_arg( 'swpm_page', $i ) );
		    $str	 = str_replace( array( '%[url]%', '%[page_num]%' ), array( $url, $i ), $tpl[ 'pagination_item' ] );
		} else
		    $str				 = str_replace( '%[page_num]%', $i, $tpl[ 'pagination_item_current' ] );
		$tpl[ 'pagination_items' ]	 .= $str;
		$i ++;
	    }
	}
    }

    if ( empty( $tpl[ 'pagination_items' ] ) ) {
	$tpl[ 'pagination' ] = '';
    }

    //Build template
    foreach ( $tpl as $key => $value ) {
	$tpl[ 'page' ] = str_replace( '_%' . $key . '%_', $value, $tpl[ 'page' ] );
    }

    return $tpl[ 'page' ];
}

function swpm_mda_get_member_item_tpl( $member, $params, $tpl ) {

    $swpm_mda_settings = get_option( 'swpm_mda_settings' );

    if ( ! isset( $swpm_mda_settings[ 'member-profile-url' ] ) ) {
	$swpm_mda_settings[ 'member-profile-url' ] = '';
    }

    $field_names = array( // those are human-readable names for tables. For example, Instead of showing 'user_name', it would be better to show "Username"
	'member_id'		 => 'Member ID',
	'user_name'		 => 'Username',
	'first_name'		 => 'First Name',
	'last_name'		 => 'Last Name',
	'membership_level'	 => 'Level',
	'account_state'		 => 'Account State',
	'full_address'		 => 'Address',
	'country'		 => 'Country',
	'phone'			 => 'Phone',
	'email'			 => 'Email',
	'member_since'		 => 'Member Since',
	'gender'		 => 'Gender',
	'company_name'		 => 'Company',
    );

    $ignored_fields = array(
	'last_accessed_from_ip'	 => '',
	'profile_image'		 => '',
    );

    $full_name = $member->full_name;

    if ( $params[ 'profile_image' ] == '1' ) {
	$member->profile_image = get_avatar( $member->email, 128, '', false, array( "force_display" => true ) );
    } else {
	$member->profile_image = '';
    }

    $member->info_lines = '';
    foreach ( $params as $key => $value ) {
	if ( $value == '1' ) {
	    if ( ! isset( $ignored_fields[ $key ] ) ) {
		//This is not an ignored field. Lets see if we need to show this in the listing entry.

		if ( ! isset( $field_names[ $key ] ) ) {
		    //This is not one of the core fields (human readable fields) that can be shown in the entry.
		    //Go to the next entry
		    continue;
		}

		$field_name = $field_names[ $key ];

		if ( isset( $member->$key ) ) {
		    $field_value = $member->$key;
		} else {
		    $field_value = ' - ';
		}
		$member->info_lines .= str_replace( array( '%[field_name]%', '%[field_value]%' ), array( $field_name, $field_value ), $tpl[ 'info_line' ] );
	    }
	}
    }

    if ( $params[ 'profile_button' ] == '1' ) {
	$view_profile_url	 = add_query_arg( 'swpm_member_id', $member->member_id, $swpm_mda_settings[ 'member-profile-url' ] );
	$member->profile_button	 = str_replace( '%[profile_url]%', $view_profile_url, $tpl[ 'profile_button' ] );
    } else {
	$member->profile_button = '';
    }

    foreach ( $member as $key => $value ) {
	$tpl[ 'member_item' ] = str_replace( '%[' . $key . ']%', $value, $tpl[ 'member_item' ] );
    }

    return $tpl[ 'member_item' ];
}
