<?php
add_action('swpm_after_main_admin_menu', 'swpm_mda_do_admin_menu');

function swpm_mda_do_admin_menu($menu_parent_slug) {
    $permission = 'manage_options';
    if (defined('SWPM_MANAGEMENT_PERMISSION')){
        $permission = SWPM_MANAGEMENT_PERMISSION;
    }    
    add_submenu_page($menu_parent_slug, __("Member Directory", 'simple-membership'), __("Member Directory", 'simple-membership'), $permission, 'swpm-mda', 'swpm_mda_admin_interface');
}

function swpm_mda_admin_interface() {

    if (isset($_POST['swpm_mda_save_settings'])) {
        $options = array(
            'member-profile-url' => esc_url($_POST['member-profile-url']),
        );
        update_option('swpm_mda_settings', $options); //store the results in WP options table
        echo '<div id="message" class="updated fade">';
        echo '<p>Settings saved!</p>';
        echo '</div>';
    }

    $swpm_mda_settings = get_option('swpm_mda_settings');
    ?>
    <div class="wrap">
        <h1>Member Directory Addon Settings</h1>
        
        <div id="poststuff">
            <div id="post-body">
        
            <p style="background: #fff6d5; border: 1px solid #d1b655; color: #3f2502; margin: 10px 0;  padding: 10px;">
                Read the <a href="https://simple-membership-plugin.com/simple-membership-member-directory-listing-addon/" target="_blank">usage documentation</a> to learn how to use the member listing addon.
            </p>
    
                <form action="" method="POST">
                    <div class="postbox">
                        <h3 class="hndle"><label for="title">Member Details Page Settings</label></h3>
                        <div class="inside">	
                            <table class="form-table">
                                <tr>
                                    <th scope="row">Member Details Page URL</th>
                                    <td>
                                        <input type="text" name="member-profile-url" size="100" value="<?php if (isset($swpm_mda_settings['member-profile-url'])) echo $swpm_mda_settings['member-profile-url']; ?>">
                                        <p class="description">When a visitor clicks on the "View Profile" link from the user listing page, the user's profile details are shown in this page. This page was automatically created for you at activation time.</p>
                                        <p class="description">If you have accidentally deleted this page then do the following. 1) Create a new page, 2) Add the [swpm_show_member_profile] shortcode to this page, 3) Enter the URL of this page in the above field and save.</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <input type="submit" name="swpm_mda_save_settings" value="Save" class="button-primary" />
                </form>                
                
            </div>
        </div>
    </div>
    <?php
}
