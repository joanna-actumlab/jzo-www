<?php

/*
  Plugin Name: SWPM Member Directory Listing
  Plugin URI: https://simple-membership-plugin.com/simple-membership-member-directory-listing-addon/
  Description: Addon for listing the members of your site on a WordPress page so your visitors can view the listing.
  Author: wp.insider
  Author URI: https://simple-membership-plugin.com/
  Version: 2.2
 */

//Slug - swpm_mda

if (!defined('ABSPATH')) {
    exit; //Exit if accessed directly
}

define('SWPM_MDA_URL', plugins_url('', __FILE__));
define('SWPM_MDA_PATH', plugin_dir_path(__FILE__));
define('SWPM_MDA_WP_SITE_URL', site_url());

include_once(SWPM_MDA_PATH . '/includes/swpm_mda_utility.php');
include_once(SWPM_MDA_PATH . '/includes/swpm_mda_show_profile.php');
include_once(SWPM_MDA_PATH . '/includes/swpm_mda_show_listing.php');
include_once(SWPM_MDA_PATH . '/includes/swpm_mda_shortcodes.php');

if (is_admin()) {
    include_once('swpm-member-directory-admin.php'); //only require menu file if admin is logged in
}

register_activation_hook(__FILE__, 'swpm_mda_activate');

function swpm_mda_activate() {
    //Do activation time tasks
    swpm_mda_create_user_details_page(); //Create the member details page (if it doesn't exist)
}
