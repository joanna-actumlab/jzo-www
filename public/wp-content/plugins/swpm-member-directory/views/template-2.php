<?php
wp_register_style('swpm_grid-style-template-2', SWPM_MDA_URL . '/css/swmp-grid-style-template-2.css'); //default template
wp_enqueue_style('swpm_grid-style-template-2');
wp_enqueue_style('dashicons');

$params = shortcode_atts(array(
    'member_id' => '0',
    'user_name' => '0',
    'first_name' => '0',
    'last_name' => '0',
    'membership_level' => '0',
    'account_state' => '0',
    'full_address' => '0',
    'company_name' => '0',
    'country' => '0',
    'phone' => '0',
    'email' => '0',
    'member_since' => '0',
    'gender' => '0',
    'items_per_page' => '30',
    'from_levels' => '',
    'from_account_status' => '',
    'sort_by' => 'unsorted',
    'sort_order' => 'asc',
    'profile_button' => '1',
    'profile_image' => '1',
    'search_box' => '1',
    'template' => '',
        ), $params, 'swpm_show_member_listing');

ob_start();
//Page
?>
<div class="swpm_mda_post-grid swpm_mda_grid">
    _%search_box%_
    <div id="swpm_mda_members-list">
        _%members_list%_
    </div>
    _%pagination%_
</div>
<?php
$tpl['page'] = ob_get_clean();
ob_start();
//Search box
?>
<form id="swpm_mda-search-form" method="GET">
    <?php
    //This will add the page_id or post_id values in the form as hidden inputs. So the search submission via GET method can work correctly.
    //So search submission on default permalink will look like this: http://www.example.com/?page_id=123&swpm_searchq=testuser
    echo swpm_mda_output_page_ids_as_hidden_input_in_form();
    ?>    
    <div class="swpm_mda_listing-search-field">
        <input type="text" class="swpm_mda_search-input" name="swpm_searchq" placeholder="<?php _e('Search...', 'simple-membership'); ?>">
        <button type="submit" class="swpm_mda_search-button" value="<?php _e('Search', 'simple-membership'); ?>" title="Search"><span class="dashicons dashicons-search"></button>
    </div>
</form>
<div class="swpm_mda_search-res-text">
    _%search_result_text%__%clear_search_button%_ 
</div>
<?php
$tpl['search_box'] = ob_get_clean();
$tpl['clear_search_button'] = ' <a href="_%clear_search_url%_">'.__('Clear Search', 'simple-membership').'</a>';
ob_start();
//Member item
?>
<div class="swpm_mda_grid-item swpm_mda_membership-lvl-%[membership_lvl_id]% swpm_mda_member-id-%[member_id]%">
    <div class="swpm_mda_profile-image">%[profile_image]%</div>
    <div class="swpm_mda_member-username">%[first_name]% %[last_name]%</div>
    <div class="swpm_mda_info-lines">
        %[info_lines]%
    </div>
    %[profile_button]%
</div>
<?php
$tpl['member_item'] = ob_get_clean();
$tpl['members_list'] = '';
$tpl['members_per_row'] = 3;
$tpl['members_row_start'] = '<div class="swpm_mda_grid-row">';
$tpl['members_row_end'] = '</div>';
$tpl['info_line'] = '<div class="swpm_mda_info-row"><span class="swpm_mda_info-label">%[field_name]%: </span><span class="swpm_mda_info-value">%[field_value]%</span></div>';
ob_start();
//Pagination
?>
<div class="swpm_mda_pagination">
    <ul>
        _%pagination_items%_        
    </ul>
</div>
<?php
$tpl['pagination'] = ob_get_clean();
//Pagination item
$tpl['pagination_item'] = '<li><a href="%[url]%">%[page_num]%</a></li>';
//Pagination item - current page
$tpl['pagination_item_current'] = '<li><span>%[page_num]%</span></li>';

//Profile button
$tpl['profile_button'] = '<div class="swpm_mda_profile-button"><a href="%[profile_url]%" class="swpm_mda_view-member-lnk" target="_blank"><button>' . __('View Profile', 'simple-membership') . '</button></a></div>';
