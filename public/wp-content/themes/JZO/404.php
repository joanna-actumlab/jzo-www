<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package JZO_Theme
 */

get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <section class="error-404 not-found">
            <span class="error-header mobile-error-header">404</span>
            <div class="error-404-img"></div>

            <div class="error-page-content">
                <span class="error-header desktop-error-header">404</span>
                <h2>BŁĄD 404</h2>
                <p>Niestety strona, którą chcesz odwiedzić nie istnieje.<br>
                    Prawdopodobnie została przeniesiona lub wpisałeś zły adres URL.</p>
                <span class="red-separator"></span>
                <h2>Przejdź do:</h2>
                <a href="/">
                    <p>Strona główna</p>
                </a>
                <a href="/search.php">
                    <p>Wyszukiwanie</p>
                </a>

            </div><!-- .page-content -->
        </section><!-- .error-404 -->

    </main><!-- #main -->
</div><!-- #primary -->
