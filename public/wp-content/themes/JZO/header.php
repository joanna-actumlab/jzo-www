<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package JZO_Theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
    <script>
        $(window).load(function() {
            // Animate loader off screen
            $(".se-pre-con").fadeOut(1000).delay(800);
        });

    </script>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div class="se-pre-con"></div>
    <div id="page" class="site">
        <a class="skip-link screen-reader-text" href="#content">
            <?php esc_html_e( 'Skip to content', 'jzo-theme' ); ?></a>

        <header id="masthead" class="site-header">
            <div class="site-branding">
                <!--
                <?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
-->
                <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                        <?php bloginfo( 'name' ); ?></a></h1>
                <?php
			else :
				?>
                <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                        <?php bloginfo( 'name' ); ?></a></p>
                <?php
			endif;
			$jzo_theme_description = get_bloginfo( 'description', 'display' );
			if ( $jzo_theme_description || is_customize_preview() ) :
				?>
                <p class="site-description">
                    <?php echo $jzo_theme_description; /* WPCS: xss ok. */ ?>
                </p>
                <?php endif; ?>
            </div><!-- .site-branding -->
            <!-- #site-navigation -->

            <nav id="site-navigation" class="main-navigation top-panel-nav">
                <?php the_custom_logo();?>
                <!--
                <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
                    <?php esc_html_e( 'Top panel', 'jzo-theme' ); ?></button>
-->
                <div class="top-panel-buttons">
                    <!--ustawienie menu w zależności od stanu zalogowania na danym szablonie strony -->

                    <?php if ( is_page_template( 'advanced-layout.php' ) ) {
   
    
   if(SwpmMemberUtils::is_member_logged_in()) {  //user zalogowany, szablon strony Dla Optyka?>
                    <a href="/dla-optykow" alt="dla-optyka" class="dla-optyka dla-optyka-white"><span></span>
                        <p>Dla optyka</p>
                    </a>
                    <a href="/edytuj-profil/" alt="profil" class="username">
                        <?php $member_id = '1';
$field_name = 'first_name';
$fname_value = SwpmMemberUtils::get_member_field_by_id($member_id, $field_name);
$field_name = 'last_name';
$lname_value = SwpmMemberUtils::get_member_field_by_id($member_id, $field_name);?></a>
                    <a href="/edytuj-profil/" class="settings"><span></span></a>
                    <a class="logout-button" href="/?swpm-logout=true" alt="wyloguj">
                        <p>Wyloguj się</p> <span></span>
                    </a>
                    <?php get_search_form(); ?>
                    <a href="/wyszukiwarka" class="search-icon">
                    </a>
                    <a class="facebook" href="#"><span></span></a>
                    <?php
}else{ //user wylogowany, szablon dla optyka
?>
                    <a href="/dla-optykow" alt="dla-optyka" class="dla-optyka dla-optyka-white"><span></span>
                        <p>Dla optyka</p>
                    </a>
                    <a class="login-button" href="/zaloguj/" alt="zaloguj">
                        <p>Zaloguj się</p><span></span>
                    </a>
                    <?php get_search_form(); ?>
                    <a href="/wyszukiwarka" class="search-icon">
                    </a>
                    <a class="facebook" href="#"><span></span></a>
                    <?php  }?>

                    <?php } else { //zalogowany, szablon strony dla klienta
    ;?>
                    <?php if(SwpmMemberUtils::is_member_logged_in()) { //zalogowany, szablon dla klienta?>
                    <a href="/dla-optykow" alt="dla-optyka" class="dla-optyka dla-optyka-white"><span></span>
                        <p>Dla optyka</p>
                    </a>
                    <a href="/edytuj-profil/" class="settings"><span></span></a>
                    <a class="logout-button" href="/?swpm-logout=true" alt="wyloguj">
                        <p>Wyloguj się</p><span></span>
                    </a>
                    <?php get_search_form(); ?>
                    <a href="/wyszukiwarka" class="search-icon">
                    </a>
                    <a class="facebook" href="#"><span></span></a>
                    <?php
}else{ //wylogowany, szablon dla klienta
?>
                    <?php get_search_form(); ?>
                    <a href="/wyszukiwarka" class="search-icon">
                    </a>
                    <a href="/dla-optykow" alt="dla-optyka" class="dla-optyka dla-optyka-red"><span></span>
                        <p>Dla optyka</p>
                    </a>
                    <a class="facebook" href="#"><span></span></a>
                    <?php  }?>

                    <?php };?>



                </div>


                <?wp_nav_menu( array(
            'theme_location' => 'top-panel',
            'menu_id' => 'top-panel',
'walker' => new WPSE_78121_Sublevel_Walker
            ) ); ?>


            </nav>

            <?php if(SwpmMemberUtils::is_member_logged_in()) { ?>
            <nav id="site-navigation" class="main-navigation second-nav-desktop">
                <?php
			wp_nav_menu( array(
				'theme_location' => 'second',
				'menu_id'        => 'second-menu',
			) );
                ?>
            </nav>
            <?php } ?>

        </header><!-- #masthead -->

        <div id="content" class="site-content">
