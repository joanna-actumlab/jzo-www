(function ($) {
    var $window = $(window);



    $(document).ready(function () {
        var height = $('.site-header').height();

        if ($window.width() < 960) {
            $('.advanced-nav .menu-toggle , .basic-nav .menu-toggle').unbind().click(function () {
                $(this).find('span').toggleClass('toggled');
                $('#main-menu').toggle();
                $('#primary-menu').toggle();
                $(".menu-main-menu-container").toggleClass('mobile-scroll');
                $(".menu-basic-menu-container").toggleClass('mobile-scroll');
                $('body').toggleClass('fixed');

            });


            $(".menu-item-has-children ").wrap("<div class='mobile-menu-wrapper'></div>");
            $(".mobile-menu-wrapper").append("<div class='mobile-menu-chevron'><span></span></div>");

            $(".mobile-menu-chevron").click(function () {
                $(this).parent().find('.menu-depth-1').slideToggle();
            });
            $(".menu-parent-item").find(".mobile-menu-chevron").click(function () {
                $(this).parent().find('.menu-depth-2').toggleClass('active');
            })
            $(".mobile-menu-chevron").click(function () {
                $(this).parent().find('.menu-no-parent').slideToggle();
                $(this).find("span").toggleClass('toggled');
            })

            $('#content').css('padding-top', (height) + 'px')
        } else {
            if ($window.width() > 960) {
                $(".mobile-menu-wrapper").children().unwrap();
                $(".mobile-menu-chevron").remove();
                $('#content').css('padding-top', (height) + 'px')
            }
        }
    });

    $window.resize(function () {
        var height = $('.site-header').height();

        if ($window.width() < 960) {
            $('.advanced-nav .menu-toggle , .basic-nav .menu-toggle').unbind().click(function () {
                $(this).find('span').toggleClass('toggled');
                $('#main-menu').toggle();
                $('#primary-menu').toggle();
                $(".menu-main-menu-container").toggleClass('mobile-scroll');
                $(".menu-basic-menu-container").toggleClass('mobile-scroll');
                $('body').toggleClass('fixed');

            });


            $(".menu-item-has-children ").wrap("<div class='mobile-menu-wrapper'></div>");
            $(".mobile-menu-wrapper").append("<div class='mobile-menu-chevron'><span></span></div>");

            $(".mobile-menu-chevron").click(function () {
                $(this).parent().find('.menu-depth-1').slideToggle();
            });
            $(".menu-parent-item").find(".mobile-menu-chevron").click(function () {
                $(this).parent().find('.menu-depth-2').toggleClass('active');
            })
            $(".mobile-menu-chevron").click(function () {
                $(this).parent().find('.menu-no-parent').slideToggle();
                $(this).find("span").toggleClass('toggled');
            })

            $('#content').css('padding-top', (height) + 'px')

        } else {
            if ($window.width() > 960) {
                $(".mobile-menu-wrapper").children().unwrap();
                $(".mobile-menu-chevron").remove();
                $('#content').css('padding-top', (height) +
                    'px')
            }
        }
    });


    $("ul:has(ul)").toggleClass("menu-depth-1");
    $("#main-menu").removeClass("menu-depth-1");
    $("#primary-menu").removeClass("menu-depth-1");
    $(".menu-depth-1").find('ul').toggleClass("menu-depth-2");
    $(".sub-menu").has(".no-children").toggleClass("menu-no-parent");



    $('.top - panel - nav > form >input').removeAttr('style');

    var forms = $('.swpm-login-form-inner >div');
    if (forms.parent().is("div")) {
        forms.unwrap();
    }
    //    $('.swpm-login-form-inner').remove();

})

(jQuery);
