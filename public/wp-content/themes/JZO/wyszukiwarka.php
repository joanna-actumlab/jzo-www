<?php
/*Template Name: Search Page
*/
?>
<?php
get_header(); ?>
<nav id="site-navigation" class="main-navigation basic-nav">

    <div class="menu-toggle"><span></span><span></span><span></span></div>

    <?wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
                'walker' => new WPSE_78121_Sublevel_Walker
			) );
			?>


</nav>


<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <div class="custom-search">
            <?php
			get_search_form();?>
        </div>
    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();