<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package JZO_Theme
 */

get_header();
?>
<nav id="site-navigation" class="main-navigation basic-nav">

    <div class="menu-toggle"><span></span><span></span><span></span></div>

    <?wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
                'walker' => new WPSE_78121_Sublevel_Walker
			) );
			?>


</nav>

<section id="primary" class="content-area">
    <main id="main" class="site-main">

        <?php if ( have_posts() ) : ?>

        <header class="page-header">
            <div class="custom-search">
                <?php
			get_search_form();?>
            </div>
            <h1 class="page-title">
                <?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Wyniki wyszukiwania dla: %s', 'jzo-theme' ), '<span class="query">' . get_search_query() . '</span>' );
					?>
            </h1>
        </header><!-- .page-header -->

        <?php
                        // Start the Loop.
                        while ( have_posts() ) : the_post();
                        ?>

        <h3><a href="<?php the_permalink() ?>">
                <?php the_title(); ?></a></h3>

        <div class="h-readmore">
            <a href="<?php the_permalink(); ?>">
                <p>Czytaj dalej...</p>
            </a>
        </div>
        <div class="red-separator"></div>
        <?php
                        endwhile;
                else :
                // If no content, include the "No posts found" template.
                get_template_part( 'content', 'none' );
                endif;
                ?>

    </main><!-- #main -->
</section><!-- #primary -->

<?php
get_sidebar();
get_footer();