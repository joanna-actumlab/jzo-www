<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package JZO_Theme
 */

?>

<section class="no-results not-found">
    <div class="custom-search">
        <?php
			get_search_form();?>
    </div>
    <header class="page-header">
        <h1 class="page-title">
            <?php esc_html_e( 'Nic nie znaleziono', 'jzo-theme' ); ?>
        </h1>
    </header><!-- .page-header -->

    <div class="page-content">


        <?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) :

			printf(
				'<p>' . wp_kses(
					/* translators: 1: link to WP admin new post page. */
					__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'jzo-theme' ),
					array(
						'a' => array(
							'href' => array(),
						),
					)
				) . '</p>',
				esc_url( admin_url( 'post-new.php' ) )
			);

		elseif ( is_search() ) :
			?>

        <p>
            <?php esc_html_e( 'Nie znaleźliśmy wyników wyszukiwania dopasowanych do wpisanej frazy. Prosimy spróbować ponownie, używając innych słów kluczowych', 'jzo-theme' ); ?>
        </p>


        <?php else :
			?>

        <p>
            <?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'jzo-theme' ); ?>
        </p>
        <?php
			get_search_form();

		endif;
		?>
    </div><!-- .page-content -->
</section><!-- .no-results -->
