<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package JZO_Theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

        <?php if ( 'post' === get_post_type() ) : ?>
        <div class="entry-meta">
            <?php
			jzo_theme_posted_on();
			jzo_theme_posted_by();
			?>
        </div><!-- .entry-meta -->
        <?php endif; ?>
    </header><!-- .entry-header -->

    <?php jzo_theme_post_thumbnail(); ?>

    <div class="entry-summary">
        <?php the_excerpt(); ?>

        <?php if ( empty( $post->post_excerpt ) ) : ?>
        <?php echo wp_kses_post( wp_trim_words( $post->post_content, 20 ) ); ?>
        <?php else : ?>
        <?php echo wp_kses_post( $post->post_excerpt ); ?>
        <?php endif; ?>
    </div><!-- .entry-summary -->

    <footer class="entry-footer">
        <?php jzo_theme_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
