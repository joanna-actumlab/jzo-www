<?php
/**
 * Template Name: Dla Optyka
 * Template Post Type: post, page, product
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package JZO_Theme
 */
 
get_header();
?>
<?php if(SwpmMemberUtils::is_member_logged_in()) { ?>
<nav id="site-navigation" class="main-navigation second-nav-mobile">
    <?php
			wp_nav_menu( array(
				'theme_location' => 'second',
				'menu_id'        => 'second-menu',
			) );
                ?>
</nav>
<?php } ?>
<nav id="site-navigation" class="main-navigation advanced-nav">

    <div class="menu-toggle"><span></span><span></span><span></span></div>
    <?wp_nav_menu( array(
            'theme_location' => 'new-menu',
            'menu_id' => 'main-menu',
'walker' => new WPSE_78121_Sublevel_Walker
            ) ); ?>


</nav>

<?php
if ( function_exists('yoast_breadcrumb') ) {
  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
}
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <div class="site-wrapper">

            <?php

// check if the flexible content field has rows of data
if( have_rows('fluid_template') ):

     // loop through the rows of data
    while ( have_rows('fluid_template') ) : the_row();?>

            <?php if( get_row_layout() == 'hero_img_1' ): ?>
            <!-- HERO IMG 1 -->
            <?php if( get_sub_field('url') ) { ?>
            <?php if( get_sub_field('img') ) { ?>
            <section id="hero_img_1">
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="hero" style="background-image: url('<?php the_sub_field('img');?>')">
                    </div>
                    <?php } ;?>
                </a>
            </section>
            <?php } ;?>
            <?php }else{;?>
            <?php if( get_sub_field('img') ) { ?>
            <section id="hero_img_1">
                <?php if( get_sub_field('img') ) { ?>
                <div class="hero" style="background-image: url('<?php the_sub_field('img');?>')">
                </div>
                <?php } ;?>
            </section>
            <?php } ;?>
            <?php };?>

            <!--2 || HERO IMAGE 2-->

            <?php elseif( get_row_layout() == 'hero_img_2' ): ?>
            <?php if( get_sub_field('url') ) { ?>
            <?php if( get_sub_field('img') ) { ?>
            <section id="hero_img_2">
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="hero" style="background-image: url('<?php the_sub_field('img');?>')">
                    </div>
                    <?php } ;?>
                </a>
            </section>
            <?php } ;?>
            <?php }else{;?>

            <?php if( get_sub_field('img') ) { ?>
            <section id="hero_img_2">
                <?php if( get_sub_field('img') ) { ?>
                <div class="hero" style="background-image: url('<?php the_sub_field('img');?>')">
                </div>
                <?php } ;?>
            </section>
            <?php };?>
            <?php };?>


            <!-- 3 || Galeria 2 kolumny - kwadratowe zdjecia-->

            <?php elseif( get_row_layout() == '2_col_gallery_1' ): ?>

            <?php if (have_rows('text_gallery')):?>
            <section id="gallery-2-col" class="gallery-2-col-1 square">
                <?php while(have_rows('text_gallery')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <div class="gallery-item">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                        <?php if( get_sub_field('header') ) { ?>
                        <h2 class="gallery-header">
                            <?php the_sub_field('header');?>
                        </h2>
                        <?php } ;?>
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                </a>
                <?php }else {;?>
                <div class="gallery-item">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php if( get_sub_field('header') ) { ?>
                    <h2 class="gallery-header">
                        <?php the_sub_field('header');?>
                    </h2>
                    <?php } ;?>
                    <?php if( get_sub_field('title') ) { ?>
                    <h3 class="gallery-title">
                        <?php the_sub_field('title');?>
                    </h3>
                    <?php } ;?>
                    <?php if( get_sub_field('description') ) { ?>
                    <p class="gallery-paragraph">
                        <?php the_sub_field('description');?>
                    </p>
                    <?php } ;?>
                </div>
                <?php };?>
                <?php endwhile;?>
            </section>
            <?php endif;?>


            <!--  4 || Galeria 2 kolumny - niskie poziome zdjecia-->

            <?php elseif( get_row_layout() == '2_col_gallery_2' ): ?>

            <?php if (have_rows('text_gallery')):?>
            <section id="gallery-2-col" class="gallery-2-col-2 landscape-wide">
                <?php while(have_rows('text_gallery')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <div class="gallery-item">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                        <?php if( get_sub_field('header') ) { ?>
                        <h2 class="gallery-header">
                            <?php the_sub_field('header');?>
                        </h2>
                        <?php } ;?>
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                </a>
                <?php }else {;?>
                <div class="gallery-item">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php if( get_sub_field('header') ) { ?>
                    <h2 class="gallery-header">
                        <?php the_sub_field('header');?>
                    </h2>
                    <?php } ;?>
                    <?php if( get_sub_field('title') ) { ?>
                    <h3 class="gallery-title">
                        <?php the_sub_field('title');?>
                    </h3>
                    <?php } ;?>
                    <?php if( get_sub_field('description') ) { ?>
                    <p class="gallery-paragraph">
                        <?php the_sub_field('description');?>
                    </p>
                    <?php } ;?>
                </div>
                <?php };?>
                <?php endwhile;?>
            </section>
            <?php endif;?>

            <!-- 5 ||  Galeria 2 kolumny - zdjecia 2:3 poziome-->

            <?php elseif( get_row_layout() == '2_col_gallery_3' ): ?>

            <?php if (have_rows('text_gallery')):?>
            <section id="gallery-2-col" class="gallery-2-col-3 landscape">
                <?php while(have_rows('text_gallery')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <div class="gallery-item">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                        <?php if( get_sub_field('header') ) { ?>
                        <h2 class="gallery-header">
                            <?php the_sub_field('header');?>
                        </h2>
                        <?php } ;?>
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                </a>
                <?php }else {;?>
                <div class="gallery-item">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php if( get_sub_field('header') ) { ?>
                    <h2 class="gallery-header">
                        <?php the_sub_field('header');?>
                    </h2>
                    <?php } ;?>
                    <?php if( get_sub_field('title') ) { ?>
                    <h3 class="gallery-title">
                        <?php the_sub_field('title');?>
                    </h3>
                    <?php } ;?>
                    <?php if( get_sub_field('description') ) { ?>
                    <p class="gallery-paragraph">
                        <?php the_sub_field('description');?>
                    </p>
                    <?php } ;?>
                </div>
                <?php };?>
                <?php endwhile;?>
            </section>
            <?php endif;?>

            <!--  6 || Galeria 3 kolumny - kwadratowe zdjecia-->

            <?php elseif( get_row_layout() == '3_col_gallery_1' ): ?>

            <?php if (have_rows('text_gallery')):?>
            <section id="gallery-3-col" class="gallery-3-col-1 square">
                <?php while(have_rows('text_gallery')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <div class="gallery-item">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                        <?php if( get_sub_field('header') ) { ?>
                        <h2 class="gallery-header">
                            <?php the_sub_field('header');?>
                        </h2>
                        <?php } ;?>
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                </a>
                <?php }else {;?>
                <div class="gallery-item">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php if( get_sub_field('header') ) { ?>
                    <h2 class="gallery-header">
                        <?php the_sub_field('header');?>
                    </h2>
                    <?php } ;?>
                    <?php if( get_sub_field('title') ) { ?>
                    <h3 class="gallery-title">
                        <?php the_sub_field('title');?>
                    </h3>
                    <?php } ;?>
                    <?php if( get_sub_field('description') ) { ?>
                    <p class="gallery-paragraph">
                        <?php the_sub_field('description');?>
                    </p>
                    <?php } ;?>
                </div>
                <?php };?>
                <?php endwhile;?>
            </section>
            <?php endif;?>


            <!--  7 || Galeria 3 kolumny - niskie poziome zdjecia-->

            <?php elseif( get_row_layout() == '3_col_gallery_2' ): ?>

            <?php if (have_rows('text_gallery')):?>
            <section id="gallery-3-col" class="gallery-3-col-2 landscape-wide">
                <?php while(have_rows('text_gallery')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <div class="gallery-item">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                        <?php if( get_sub_field('header') ) { ?>
                        <h2 class="gallery-header">
                            <?php the_sub_field('header');?>
                        </h2>
                        <?php } ;?>
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                </a>
                <?php }else {;?>
                <div class="gallery-item">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php if( get_sub_field('header') ) { ?>
                    <h2 class="gallery-header">
                        <?php the_sub_field('header');?>
                    </h2>
                    <?php } ;?>
                    <?php if( get_sub_field('title') ) { ?>
                    <h3 class="gallery-title">
                        <?php the_sub_field('title');?>
                    </h3>
                    <?php } ;?>
                    <?php if( get_sub_field('description') ) { ?>
                    <p class="gallery-paragraph">
                        <?php the_sub_field('description');?>
                    </p>
                    <?php } ;?>
                </div>
                <?php };?>
                <?php endwhile;?>
            </section>
            <?php endif;?>

            <!--  8 || Galeria 3 kolumny - zdjecia 2:3 poziom-->

            <?php elseif( get_row_layout() == '3_col_gallery_3' ): ?>

            <?php if (have_rows('text_gallery')):?>
            <section id="gallery-3-col" class="gallery-3-col-3 landscape">
                <?php while(have_rows('text_gallery')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <div class="gallery-item">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                        <?php if( get_sub_field('header') ) { ?>
                        <h2 class="gallery-header">
                            <?php the_sub_field('header');?>
                        </h2>
                        <?php } ;?>
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                </a>
                <?php }else {;?>
                <div class="gallery-item">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php if( get_sub_field('header') ) { ?>
                    <h2 class="gallery-header">
                        <?php the_sub_field('header');?>
                    </h2>
                    <?php } ;?>
                    <?php if( get_sub_field('title') ) { ?>
                    <h3 class="gallery-title">
                        <?php the_sub_field('title');?>
                    </h3>
                    <?php } ;?>
                    <?php if( get_sub_field('description') ) { ?>
                    <p class="gallery-paragraph">
                        <?php the_sub_field('description');?>
                    </p>
                    <?php } ;?>
                </div>
                <?php };?>

                <?php endwhile;?>
            </section>
            <?php endif;?>

            <!--  9 || Galeria 3 kolumny - zdjecia 3:2 pion-->

            <?php elseif( get_row_layout() == '3_col_gallery_4' ): ?>

            <?php if (have_rows('text_gallery')):?>
            <section id="gallery-3-col" class="gallery-3-col-4 portrait">
                <?php while(have_rows('text_gallery')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>

                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <div class="gallery-item">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                        <?php if( get_sub_field('header') ) { ?>
                        <h2 class="gallery-header">
                            <?php the_sub_field('header');?>
                        </h2>
                        <?php } ;?>
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                </a>
                <?php }else {;?>
                <div class="gallery-item">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php if( get_sub_field('header') ) { ?>
                    <h2 class="gallery-header">
                        <?php the_sub_field('header');?>
                    </h2>
                    <?php } ;?>
                    <?php if( get_sub_field('title') ) { ?>
                    <h3 class="gallery-title">
                        <?php the_sub_field('title');?>
                    </h3>
                    <?php } ;?>
                    <?php if( get_sub_field('description') ) { ?>
                    <p class="gallery-paragraph">
                        <?php the_sub_field('description');?>
                    </p>
                    <?php } ;?>
                </div>
                <?php };?>
                <?php endwhile;?>
            </section>
            <?php endif;?>

            <!--  10 || Galeria 4 kolumny - kwadratowe zdjecia-->

            <?php elseif( get_row_layout() == '4_col_gallery_1' ): ?>

            <?php if (have_rows('text_gallery')):?>
            <section id="gallery-4-col" class="gallery-4-col-1 square">
                <?php while(have_rows('text_gallery')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <div class="gallery-item">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                        <?php if( get_sub_field('header') ) { ?>
                        <h2 class="gallery-header">
                            <?php the_sub_field('header');?>
                        </h2>
                        <?php } ;?>
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                </a>
                <?php }else {;?>
                <div class="gallery-item">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php if( get_sub_field('header') ) { ?>
                    <h2 class="gallery-header">
                        <?php the_sub_field('header');?>
                    </h2>
                    <?php } ;?>
                    <?php if( get_sub_field('title') ) { ?>
                    <h3 class="gallery-title">
                        <?php the_sub_field('title');?>
                    </h3>
                    <?php } ;?>
                    <?php if( get_sub_field('description') ) { ?>
                    <p class="gallery-paragraph">
                        <?php the_sub_field('description');?>
                    </p>
                    <?php } ;?>
                </div>
                <?php };?>
                <?php endwhile;?>
            </section>
            <?php endif;?>

            <!--  11 || Galeria 4 kolumny - niskie poziome zdjecia-->

            <?php elseif( get_row_layout() == '4_col_gallery_2' ): ?>

            <?php if (have_rows('text_gallery')):?>
            <section id="gallery-4-col" class="gallery-4-col-2 landscape-wide">
                <?php while(have_rows('text_gallery')) : the_row();?>

                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <div class="gallery-item">
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <h2 class="gallery-header">
                            <?php the_sub_field('header');?>
                        </h2>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                    </div>
                </a>
                <?php endwhile;?>
            </section>
            <?php endif;?>


            <!--  12 || Galeria 4 kolumny - zdjecia 2:3 poziome-->

            <?php elseif( get_row_layout() == '4_col_gallery_3' ): ?>

            <?php if (have_rows('text_gallery')):?>
            <section id="gallery-4-col" class="gallery-4-col-3 landscape">
                <?php while(have_rows('text_gallery')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <div class="gallery-item">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                        <?php if( get_sub_field('header') ) { ?>
                        <h2 class="gallery-header">
                            <?php the_sub_field('header');?>
                        </h2>
                        <?php } ;?>
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                </a>
                <?php }else {;?>
                <div class="gallery-item">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php if( get_sub_field('header') ) { ?>
                    <h2 class="gallery-header">
                        <?php the_sub_field('header');?>
                    </h2>
                    <?php } ;?>
                    <?php if( get_sub_field('title') ) { ?>
                    <h3 class="gallery-title">
                        <?php the_sub_field('title');?>
                    </h3>
                    <?php } ;?>
                    <?php if( get_sub_field('description') ) { ?>
                    <p class="gallery-paragraph">
                        <?php the_sub_field('description');?>
                    </p>
                    <?php } ;?>
                </div>
                <?php };?>
                <?php endwhile;?>
            </section>
            <?php endif;?>


            <!--  13 ||GALERIA 5 KOLUMN - kwadratowe zdjęcia -->

            <?php elseif( get_row_layout() == '5_col_gallery_1' ): ?>
            <?php if (have_rows('text_gallery')):?>
            <section id="gallery-5-col" class="gallery-5-col-1 square">
                <?php while(have_rows('text_gallery')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <div class="gallery-item">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                        <?php if( get_sub_field('header') ) { ?>
                        <h2 class="gallery-header">
                            <?php the_sub_field('header');?>
                        </h2>
                        <?php } ;?>
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                </a>
                <?php }else {;?>
                <div class="gallery-item">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php if( get_sub_field('header') ) { ?>
                    <h2 class="gallery-header">
                        <?php the_sub_field('header');?>
                    </h2>
                    <?php } ;?>
                    <?php if( get_sub_field('title') ) { ?>
                    <h3 class="gallery-title">
                        <?php the_sub_field('title');?>
                    </h3>
                    <?php } ;?>
                    <?php if( get_sub_field('description') ) { ?>
                    <p class="gallery-paragraph">
                        <?php the_sub_field('description');?>
                    </p>
                    <?php } ;?>
                </div>
                <?php };?>
                <?php endwhile;?>
            </section>
            <?php endif;?>

            <!-- 14 || GALERIA 5 KOLUMN - niskie poziome zdjęcia-->

            <?php elseif( get_row_layout() == '5_col_gallery_2' ): ?>
            <?php if (have_rows('text_gallery')):?>
            <section id="gallery-5-col" class="gallery-5-col-2 landscape-wide">
                <?php while(have_rows('text_gallery')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <div class="gallery-item">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                        <?php if( get_sub_field('header') ) { ?>
                        <h2 class="gallery-header">
                            <?php the_sub_field('header');?>
                        </h2>
                        <?php } ;?>
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                </a>
                <?php }else {;?>
                <div class="gallery-item">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php if( get_sub_field('header') ) { ?>
                    <h2 class="gallery-header">
                        <?php the_sub_field('header');?>
                    </h2>
                    <?php } ;?>
                    <?php if( get_sub_field('title') ) { ?>
                    <h3 class="gallery-title">
                        <?php the_sub_field('title');?>
                    </h3>
                    <?php } ;?>
                    <?php if( get_sub_field('description') ) { ?>
                    <p class="gallery-paragraph">
                        <?php the_sub_field('description');?>
                    </p>
                    <?php } ;?>
                </div>
                <?php };?>
                <?php endwhile;?>
            </section>
            <?php endif;?>

            <!-- 15 || Elastyczna galeria z wyśrodkowanym ostatnim rzędem-->

            <?php elseif( get_row_layout() == 'gallery_flex' ): ?>
            <?php if (have_rows('gallery_flex')):?>
            <section id="icon-gallery-center" class="icon-gallery-center">
                <?php while(have_rows('gallery_flex')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <div class="gallery-item">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                        <?php if( get_sub_field('header') ) { ?>
                        <h2 class="gallery-header">
                            <?php the_sub_field('header');?>
                        </h2>
                        <?php } ;?>
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                </a>
                <?php }else {;?>
                <div class="gallery-item">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php if( get_sub_field('header') ) { ?>
                    <h2 class="gallery-header">
                        <?php the_sub_field('header');?>
                    </h2>
                    <?php } ;?>
                    <?php if( get_sub_field('title') ) { ?>
                    <h3 class="gallery-title">
                        <?php the_sub_field('title');?>
                    </h3>
                    <?php } ;?>
                    <?php if( get_sub_field('description') ) { ?>
                    <p class="gallery-paragraph">
                        <?php the_sub_field('description');?>
                    </p>
                    <?php } ;?>
                </div>
                <?php };?>
                <?php endwhile;?>
            </section>
            <?php endif;?>

            <!-- 16 ||  KOLUMNY TEKSTU -->

            <?php elseif( get_row_layout() == 'text_columns' ): ?>
            <?php if (have_rows('text_editor_group')):?>
            <section id="text-column-group" class="text-column-group">
                <?php while(have_rows('text_editor_group')) : the_row();?>
                <div class="text-column">
                    <?php the_sub_field('text_column');?>
                </div>
                <?php endwhile;?>
            </section>
            <?php endif;?>

            <!-- 17 || CZERWONY NAGŁOWEK -->

            <?php elseif( get_row_layout() == 'red_header' ): ?>
            <?php if( get_sub_field('red_header') ) { ?>
            <section id="red-header">
                <h1 class="red-header">
                    <?php the_sub_field('red_header');?>
                </h1>
            </section>
            <?php };?>

            <!-- 18 ||  BIALY NAGLOWEK / TEKST NA CZERWONYM TLE -->
            <?php elseif( get_row_layout() == 'red_jumbotron' ): ?>
            <?php if( get_sub_field('url') ) { ?>
            <?php if( get_sub_field('jumbotron_header') ) { ?>
            <section id="red-jumbotron" class="red-jumbotron">
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <?php if( get_sub_field('jumbotron_header') ) { ?>
                    <h1>
                        <?php the_sub_field('jumbotron_header');?>
                    </h1>
                    <?php } ;?>
                    <?php if( get_sub_field('jumbotron_description') ) { ?>
                    <p>
                        <?php the_sub_field('jumbotron_description');?>
                    </p>
                    <?php } ;?>
                </a>
            </section>
            <?php } ;?>
            <?php }else{;?>
            <?php if( get_sub_field('jumbotron_header') ) { ?>
            <section id="red-jumbotron" class="red-jumbotron">
                <?php if( get_sub_field('jumbotron_header') ) { ?>
                <h1>
                    <?php the_sub_field('jumbotron_header');?>
                </h1>
                <?php } ;?>
                <?php if( get_sub_field('jumbotron_description') ) { ?>
                <p>
                    <?php the_sub_field('jumbotron_description');?>
                </p>
                <?php } ;?>
            </section>
            <?php } ;?>
            <?php } ;?>

            <!--  19 || SEPARATOR -->

            <?php elseif( get_row_layout() == 'red_separator' ): ?>
            <section id="red-separator" class="red-separator">

                <?php the_sub_field('red-separator');?>

            </section>

            <!--  20 || TEKST I GALERIA 1:1 -->

            <?php elseif( get_row_layout() == 'text_and_grid_gallery_row' ): ?>
            <?php if (have_rows('text_and_grid_gallery_row')):?>


            <section id="text_and_grid_gallery_row" class="text-grid-row">
                <?php while(have_rows('text_and_grid_gallery_row')) : the_row();?>
                <div class="paragraph">
                    <?php the_sub_field('paragraph');?>
                </div>

                <?php if (have_rows('gallery')):?>
                <div class="gallery">
                    <?php while(have_rows('gallery')) : the_row();?>

                    <?php if( get_sub_field('url') ) { ?>
                    <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                        <div class="gallery-item">
                            <?php if( get_sub_field('img') ) { ?>
                            <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                            <?php } ;?>
                            <?php if( get_sub_field('title') ) { ?>
                            <h3 class="gallery-title">
                                <?php the_sub_field('title');?>
                            </h3>
                            <?php } ;?>
                            <?php if( get_sub_field('description') ) { ?>
                            <p class="gallery-desc">
                                <?php the_sub_field('description');?>
                            </p>
                            <?php } ;?>
                        </div>
                    </a>

                    <?php } else{ ;?>
                    <div class="gallery-item">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-desc">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                    <?php };?>
                    <?php endwhile;?>
                </div>
                <?php endif;?>
                <?php endwhile;?>
            </section>

            <?php endif;?>

            <!-- 21 ||  TEKST I OBRAZ 1:2 -->

            <?php elseif( get_row_layout() == 'text_and_img_row_1' ): ?>
            <?php if (have_rows('text_and_img_row_1')):?>

            <section id="text_and_img_row" class="text-img-row-1">
                <?php while(have_rows('text_and_img_row_1')) : the_row();?>
                <?php if( get_sub_field('paragraph') ) { ?>
                <div class="paragraph">
                    <?php the_sub_field('paragraph');?>
                </div>
                <?php };?>


                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="
                <?php the_sub_field('alt_tag');?>">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php };?>
                </a>
                <?php } else{ ;?>
                <?php if( get_sub_field('img') ) { ?>
                <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                <?php };?>
                <?php };?>




                <?php endwhile;?>
            </section>

            <?php endif;?>

            <!--  22 || TEKST I OBRAZ 2:1 -->

            <?php elseif( get_row_layout() == 'text_and_img_row_2' ): ?>
            <?php if (have_rows('text_and_img_row_2')):?>

            <section id="text_and_img_row" class="text-img-row-2">
                <?php while(have_rows('text_and_img_row_2')) : the_row();?>
                <?php if( get_sub_field('paragraph') ) { ?>
                <div class="paragraph">
                    <?php the_sub_field('paragraph');?>
                </div>
                <?php };?>


                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="
                <?php the_sub_field('alt_tag');?>">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php };?>
                </a>
                <?php } else{ ;?>
                <?php if( get_sub_field('img') ) { ?>
                <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                <?php };?>
                <?php };?>

                <?php endwhile;?>
            </section>

            <?php endif;?>


            <!-- 23 ||  TEKST I OBRAZ 1:1 -->

            <?php elseif( get_row_layout() == 'text_and_img_row_3' ): ?>
            <?php if (have_rows('text_and_img_row_3')):?>

            <section id="text_and_img_row" class="text-img-row-3">
                <?php while(have_rows('text_and_img_row_3')) : the_row();?>
                <?php if( get_sub_field('paragraph') ) { ?>
                <div class="paragraph">
                    <?php the_sub_field('paragraph');?>
                </div>
                <?php };?>


                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="
                <?php the_sub_field('alt_tag');?>">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php };?>
                </a>
                <?php } else{ ;?>
                <?php if( get_sub_field('img') ) { ?>
                <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                <?php };?>
                <?php };?>

                <?php endwhile;?>
            </section>

            <?php endif;?>


            <!--  24 || OBRAZ I TEKST 1:1 -->

            <?php elseif( get_row_layout() == 'img_and_text_row_1' ): ?>
            <?php if (have_rows('img_and_text_row_1')):?>

            <section id="text_and_img_row" class="img-text-row-1">
                <?php while(have_rows('img_and_text_row_1')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="
                <?php the_sub_field('alt_tag');?>">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php };?>
                </a>
                <?php } else{ ;?>
                <?php if( get_sub_field('img') ) { ?>
                <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                <?php };?>
                <?php };?>
                <?php if( get_sub_field('paragraph') ) { ?>
                <div class="paragraph">
                    <?php the_sub_field('paragraph');?>
                </div>
                <?php };?>
                <?php endwhile;?>
            </section>

            <?php endif;?>

            <!--  25 ||Obraz i kolumna tekstu 1:2 -->

            <?php elseif( get_row_layout() == 'img_and_text_row_2' ): ?>
            <?php if (have_rows('img_and_text_row_2')):?>

            <section id="text_and_img_row" class="img-text-row-2">
                <?php while(have_rows('img_and_text_row_2')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="
                <?php the_sub_field('alt_tag');?>">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php };?>
                </a>
                <?php } else{ ;?>
                <?php if( get_sub_field('img') ) { ?>
                <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                <?php };?>
                <?php };?>
                <?php if( get_sub_field('paragraph') ) { ?>
                <div class="paragraph">
                    <?php the_sub_field('paragraph');?>
                </div>
                <?php };?>

                <?php endwhile;?>
            </section>

            <?php endif;?>

            <!-- 26 || Obraz i kolumna tekstu 2:1 -->

            <?php elseif( get_row_layout() == 'img_and_text_row_3' ): ?>
            <?php if (have_rows('img_and_text_row_3')):?>

            <section id="text_and_img_row" class="img-text-row-3">
                <?php while(have_rows('img_and_text_row_3')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="
                <?php the_sub_field('alt_tag');?>">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php };?>
                </a>
                <?php } else{ ;?>
                <?php if( get_sub_field('img') ) { ?>
                <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                <?php };?>
                <?php };?>
                <?php if( get_sub_field('paragraph') ) { ?>
                <div class="paragraph">
                    <?php the_sub_field('paragraph');?>
                </div>
                <?php };?>

                <?php endwhile;?>
            </section>

            <?php endif;?>

            <!-- 27 || Galeria produktu -->
            <?php elseif( get_row_layout() == 'product_gallery' ): ?>
            <?php if (have_rows('product_gallery_item')):?>
            <section id="product_gallery" class="product-gallery">
                <?php while(have_rows('product_gallery_item')) : the_row();?>
                <?php if( get_sub_field('url') ) { ?>
                <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                    <div class="gallery-item">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                        <?php if( get_sub_field('name') ) { ?>
                        <div class="paragraph">
                            <?php if( get_sub_field('name') ) { ?>
                            <h3 class="name">
                                <?php the_sub_field('name');?>
                            </h3>
                            <?php } ;?>
                            <?php if( get_sub_field('description') ) { ?>
                            <p class="">
                                <?php the_sub_field('description');?>
                            </p>
                            <?php } ;?>
                        </div>
                        <?php } ;?>
                    </div>
                </a>
                <?php }else {;?>
                <div class="gallery-item">
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php if( get_sub_field('name') ) { ?>
                    <div class="paragraph">
                        <?php if( get_sub_field('name') ) { ?>
                        <h3 class="name">
                            <?php the_sub_field('name');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                    <?php } ;?>

                </div>
                <?php };?>
                <?php endwhile;?>
            </section>
            <?php endif;?>

            <!-- 28 || Szachownica zdjecie i tekst-->
            <?php elseif( get_row_layout() == 'chess_img_text' ): ?>
            <?php if (have_rows('chess_img_text')):?>
            <section id="chess_grid" class="chess-grid">
                <?php while(have_rows('chess_img_text')) : the_row();?>
                <?php if (have_rows('chess-left')):?>
                <div class="chess-left">
                    <?php while(have_rows('chess-left')) : the_row();?>
                    <?php if( get_sub_field('img') ) { ?>

                    <?php if( get_sub_field('url') ) { ?>
                    <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                    </a>
                    <?php } else{ ;?>
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php } ;?>
                    <div class="paragraph">
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>


                    <?php };?>

                    <?php endwhile;?>
                </div>
                <?php endif;?>
                <?php if (have_rows('chess-right')):?>
                <div class="chess-right">
                    <?php while(have_rows('chess-right')) : the_row();?>
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="paragraph">
                        <?php if( get_sub_field('title') ) { ?>
                        <h3 class="gallery-title">
                            <?php the_sub_field('title');?>
                        </h3>
                        <?php } ;?>
                        <?php if( get_sub_field('description') ) { ?>
                        <p class="gallery-paragraph">
                            <?php the_sub_field('description');?>
                        </p>
                        <?php } ;?>
                    </div>
                    <?php if( get_sub_field('url') ) { ?>
                    <a href="<?php the_sub_field('url');?>" alt="<?php the_sub_field('alt_tag');?>">
                        <?php if( get_sub_field('img') ) { ?>
                        <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                        <?php } ;?>
                    </a>
                    <?php } else{ ;?>
                    <?php if( get_sub_field('img') ) { ?>
                    <div class="img" style="background-image: url('<?php the_sub_field('img');?>')"></div>
                    <?php } ;?>
                    <?php } ;?>

                    <?php };?>

                    <?php endwhile;?>
                </div>
                <?php endif;?>


                <?php endwhile;?>
            </section>
            <?php endif;?>
            <?php elseif( get_row_layout() == 'blank' ): ?>
            <div class="blank-space"></div>




            <?php endif;

    endwhile;

else :

    // no layouts found

endif;

?>
        </div>
    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
