<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package JZO_Theme
 */

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer">
    <div class="site-info">
        <p>
            <?php the_field('adres', 491);?><br>
            <?php  the_field('kod-pocztowy', 491);?><br>
            <?php the_field('e-mail', 491);?>
        </p>
        <p>
            <?php the_field('biuro', 491);?><br>
            <?php the_field('numer-1', 491);?>
            <br>
            <?php the_field('numer-2', 491);?>
        </p>
    </div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>
